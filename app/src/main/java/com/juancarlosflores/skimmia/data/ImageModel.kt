package com.juancarlosflores.skimmia.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageModel(val name: String? = "", val filePath: String?) : Parcelable