package com.juancarlosflores.skimmia.gallery.task

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import com.juancarlosflores.skimmia.data.ImageModel

class LoadPhotoTask(listener: BaseAsyncTask.ProgressListener, val context: Context): BaseAsyncTask(listener) {
    override fun doInBackground(vararg params: Void?): Any? {
        var result: Any?
        var listImages = arrayListOf<ImageModel>()
        val uri: Uri
        val cursor: Cursor?
        val column_index_data: Int?
        val column_index_folder_name: Int?
        val listOfAllImages = ArrayList<String>()
        var absolutePathOfImage: String? = null
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        try {
            val projection = arrayOf(
                MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME
            )

            cursor = context.contentResolver.query(
                uri, projection, null,
                null, null
            )

            column_index_data = cursor?.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
            column_index_folder_name = cursor!!
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data!!)
                listImages.add(ImageModel("", absolutePathOfImage))
                println("-----Path: $absolutePathOfImage")
                if (listImages.size == 10) break
            }
        } catch (e: Exception) {
            e.stackTrace
        }

        return if (listImages.isNullOrEmpty()) return "Error al obtener Imágenes" else listImages
    }
}