package com.juancarlosflores.skimmia.gallery

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.github.chrisbanes.photoview.PhotoView
import com.juancarlosflores.skimmia.R
import com.juancarlosflores.skimmia.data.ImageModel

class ImageDetailFragment() : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(requireContext())
                .inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var imageModel: ImageModel? = arguments?.getParcelable(EXTRA_IMAGE)
        var transitionName: String? = arguments?.getString(EXTRA_TRANSITION_NAME)!!

        val imageView = view.findViewById<PhotoView>(R.id.detail_image)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.transitionName = transitionName
        }

        Glide.with(requireContext())
            .load(imageModel?.filePath)
            .into(object: SimpleTarget<Drawable>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable>?
                ) {
                    startPostponedEnterTransition()
                    imageView.setImageDrawable(resource)
                }
            });
    }

    companion object {
        const val EXTRA_IMAGE = "image_item"
        const val EXTRA_TRANSITION_NAME = "transition_name"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param EXTRA_IMAGE Parameter 1.
         * @param EXTRA_TRANSITION_NAME Parameter 2.
         * @return A new instance of fragment ImageDetailFragment.
         */
        @JvmStatic
        fun newInstance(image: ImageModel?, transitionName: String?) =
            ImageDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_IMAGE, image)
                    putString(EXTRA_TRANSITION_NAME, transitionName)
                }
            }
    }
}