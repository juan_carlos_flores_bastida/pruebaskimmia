package com.juancarlosflores.skimmia.gallery

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.juancarlosflores.skimmia.R
import com.juancarlosflores.skimmia.data.ImageModel

class GalleryAdapter: RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    private var galleryItemClickListener: GalleryItemClickListener? = null
    private var galleryImageList: ArrayList<ImageModel>? = null

    constructor(galleryImageList: ArrayList<ImageModel>?, galleryItemClickListener: GalleryItemClickListener?) {
        this.galleryImageList = galleryImageList
        this.galleryItemClickListener = galleryItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        return GalleryViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.gallery_item, parent, false))
    }

    override fun getItemCount(): Int {
        return galleryImageList?.size ?: 0
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        val imageModel = galleryImageList?.get(position)

        Glide.with(holder.galleryImageView.context).load(imageModel?.filePath)
            .thumbnail(0.5f)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.galleryImageView)

        holder.galleryImageView.setOnClickListener {
            galleryItemClickListener!!.onGalleryItemClickListener(
                holder.adapterPosition,
                imageModel!!,
                holder.galleryImageView
            )
        }
    }


    class GalleryViewHolder(view: View) : ViewHolder(view) {
        var galleryImageView: ImageView = view.findViewById(R.id.gallery_image)
    }
}