package com.juancarlosflores.skimmia.gallery

import android.widget.ImageView
import com.juancarlosflores.skimmia.data.ImageModel

interface GalleryItemClickListener {
    fun onGalleryItemClickListener(position: Int, imageModel: ImageModel, imageView: ImageView)
}