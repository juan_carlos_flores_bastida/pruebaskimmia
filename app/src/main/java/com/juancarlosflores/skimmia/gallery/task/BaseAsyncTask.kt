package com.juancarlosflores.skimmia.gallery.task

import android.os.AsyncTask
import com.juancarlosflores.skimmia.data.ImageModel

abstract class BaseAsyncTask(private val listener: ProgressListener): AsyncTask<Void, Void, Any?>() {

    interface ProgressListener {
        // callback for start
        fun onStarted()

        // callback on success
        fun onCompleted(listImages: ArrayList<ImageModel>)

        // callback on error
        fun onError(errorMessage: String?)

    }
    override fun onPreExecute() {
        listener.onStarted()

    }

    override fun onPostExecute(result: Any?) {
        super.onPostExecute(result)
        when(result){
            is String -> listener.onError(result as String?)
            else -> listener.onCompleted(result as ArrayList<ImageModel>)
        }
        /*if (null != result) {
            listener.onError(result as String?)
        } else {
            listener.onCompleted(result as ArrayList<ImageModel>)
        }*/
    }
}