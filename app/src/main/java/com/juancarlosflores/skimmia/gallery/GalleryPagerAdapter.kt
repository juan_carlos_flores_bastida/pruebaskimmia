package com.juancarlosflores.skimmia.gallery

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.juancarlosflores.skimmia.data.ImageModel

class GalleryPagerAdapter(val fm: FragmentManager, val images: ArrayList<ImageModel>?): FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var image = images?.get(position)
        return ImageDetailFragment.newInstance(image, image?.name)
    }

    override fun getCount(): Int {
        return images?.size ?: 0
    }
}