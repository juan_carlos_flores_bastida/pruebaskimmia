package com.juancarlosflores.skimmia.gallery

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juancarlosflores.skimmia.R
import com.juancarlosflores.skimmia.data.ImageModel
import com.juancarlosflores.skimmia.gallery.task.BaseAsyncTask
import com.juancarlosflores.skimmia.gallery.task.LoadPhotoTask

class GalleryFragment() : Fragment(), GalleryItemClickListener {

    val TAG = GalleryFragment::class.simpleName

    lateinit var _listImages: ArrayList<ImageModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        readImages(this)
        /*val galleryAdapter = GalleryAdapter(readImages(), this)
        var recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        val gridLayoutManager = GridLayoutManager(requireContext(), 2)
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = galleryAdapter*/
    }

    fun readImages(galleryItemClickListener: GalleryItemClickListener){
        LoadPhotoTask(object : BaseAsyncTask.ProgressListener {
            override fun onStarted() {
                val progress = view?.findViewById<ProgressBar>(R.id.progress_bar)
                progress?.visibility = View.VISIBLE
                println("Inicial la carga de imágenes")
            }

            override fun onCompleted(listImages: ArrayList<ImageModel>) {
                val galleryAdapter = GalleryAdapter(listImages, galleryItemClickListener)
                var recyclerView = view?.findViewById<RecyclerView>(R.id.recycler_view)
                val gridLayoutManager = GridLayoutManager(requireContext(), 2)
                recyclerView?.layoutManager = gridLayoutManager
                recyclerView?.adapter = galleryAdapter
                _listImages = listImages
                val progress = view?.findViewById<ProgressBar>(R.id.progress_bar)
                progress?.visibility = View.GONE
                println("imagenes descargadas correctamente<--------------")
            }

            override fun onError(errorMessage: String?) {
                val progress = view?.findViewById<ProgressBar>(R.id.progress_bar)
                progress?.visibility = View.GONE
                println("Error al cargar imagenes")
            }

        }, requireContext()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        /*var listImages = arrayListOf<ImageModel>()
        val uri: Uri
        val cursor: Cursor?
        val column_index_data: Int?
        val column_index_folder_name: Int?
        val listOfAllImages = ArrayList<String>()
        var absolutePathOfImage: String? = null
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        try {
            val projection = arrayOf(
                MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME
            )

            cursor = activity!!.contentResolver.query(
                uri, projection, null,
                null, null
            )

            column_index_data = cursor?.getColumnIndexOrThrow(MediaColumns.DATA)
            column_index_folder_name = cursor!!
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data!!)
                listImages.add(ImageModel("", absolutePathOfImage))
                println("-----Path: $absolutePathOfImage")
            }
        } catch (e: Exception) {
            e.stackTrace
        }
        return listImages*/
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            GalleryFragment()
    }

    override fun onGalleryItemClickListener(
        position: Int,
        imageModel: ImageModel,
        imageView: ImageView
    ) {
//        var imageDetailFragment = ImageDetailFragment.newInstance(imageModel, imageModel?.name)
        var galleryViewPagerFragment = GalleryViewPagerFragment.newInstance(position, _listImages)

        fragmentManager
            ?.beginTransaction()
            //?.addSharedElement(imageView, ViewCompat.getTransitionName(imageView)!!)
            ?.addToBackStack(TAG)
            ?.replace(R.id.content, galleryViewPagerFragment)
            ?.commit()
    }
}