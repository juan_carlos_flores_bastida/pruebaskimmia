
package com.juancarlosflores.skimmia.gallery

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.juancarlosflores.skimmia.R
import com.juancarlosflores.skimmia.data.ImageModel

class GalleryViewPagerFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(requireContext())
                .inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gallery_view_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var currentItem = arguments?.getInt(EXTRA_INITIAL_POS)
        var images: ArrayList<ImageModel> = arguments?.getParcelableArrayList(EXTRA_IMAGE)!!

        var galleryAdapter = GalleryPagerAdapter(childFragmentManager, images)

        val viewPager = view.findViewById<ViewPager>(R.id.image_view_pager)
        viewPager.adapter = galleryAdapter
        viewPager.currentItem = currentItem!!
    }

    companion object {
        const val EXTRA_INITIAL_POS = "initial_pos"
        const val EXTRA_IMAGE = "images"
        @JvmStatic
        fun newInstance(current: Int, images: ArrayList<ImageModel>) =
            GalleryViewPagerFragment().apply {
                arguments = Bundle().apply {
                    putInt(EXTRA_INITIAL_POS, current)
                    putParcelableArrayList(EXTRA_IMAGE, images)
                }
            }
    }
}